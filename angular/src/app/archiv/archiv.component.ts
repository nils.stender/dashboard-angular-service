import { Component, OnInit, ViewChild, forwardRef } from '@angular/core';
import { ErrorService } from '../services/mysql.service';
import { mysqlErrHist, inputBox } from '../services/mysql';
import {SelectItem} from 'primeng/api';
import { Chart, BaseChartDirective } from 'chart.js';

@Component({
  selector: 'app-archiv',
  templateUrl: './archiv.component.html',
  styleUrls: ['./archiv.component.css']
})
export class ArchivComponent implements OnInit {

  idsForSelection: SelectItem[];
  selectedIds: inputBox[];
  timespanBegin: Date = new Date(new Date().setDate(new Date().getDate()-7));
  timespanEnd: Date = new Date(new Date().setDate(new Date().getDate()+1));
  data: any;
  listData = [];
  listLabel = [];
  listId = [];
  listTime = [];
  @ViewChild(forwardRef(() => BaseChartDirective))
  public mychart1: BaseChartDirective;
  @ViewChild(forwardRef(() => BaseChartDirective))
  public mychart2: BaseChartDirective;


  constructor(private errorService: ErrorService) {
    this.idsForSelection = [
      {label:'Fehler-ID 1', value:{label:'Fehler-ID 1', id:1, sensoren: '10039,10040', area: 'A', beschr: 'Lichtschranken am Förderband Lager sind gleichzeitig unterbrochen.'}},
      {label:'Fehler-ID 2', value:{label:'Fehler-ID 2', id:2, sensoren: '10031,10048', area: 'A', beschr: 'Leere WT wird zum Hochregallager befördert.'}},
      {label:'Fehler-ID 3', value:{label:'Fehler-ID 3', id:3, sensoren: '20037,20030,20033', area: 'C', beschr: 'Säge bearbeitet das Teil nicht, Kommunikationsausfall.'}},
      {label:'Fehler-ID 4', value:{label:'Fehler-ID 4', id:4, sensoren: '10025,10031', area: 'B', beschr: 'Sauggreifer befördert kein Teil zurück.'}},
      {label:'Fehler-ID 5', value:{label:'Fehler-ID 5', id:5, sensoren: '20051,20053,20054,20055', area: 'D', beschr: 'Sortierstrecke konnte Teil nicht zuordnen.'}},
      {label:'Fehler-ID 6', value:{label:'Fehler-ID 6', id:6, sensoren: '10025,10030,20028', area: 'C', beschr: 'Die Lichtschranke am Brennofen registriert nicht, dass ein Werkstück auf dem Schieber liegt.'}},
      {label:'Fehler-ID 7', value:{label:'Fehler-ID 7', id:7, sensoren: '20028,20034,20038,20042', area: 'C', beschr: 'Das Tor des Brennofen öfnet nicht.'}}
    ];    
  }


  ngOnInit() { 
    this.mychart1 =  new Chart('canvas-left', {
      type: 'bar',
      data: {
        labels: this.listLabel,
        datasets: [
          {
            data: this.listData,
            borderColor: '#0000FF',
            backgroundColor: '#0000FF',
            hoverBackgroundColor: '#b3e7fb',
            hoverBorderColor: '#b3e7fb'
          }
        ]
      },
      options: {
        title: {
          display: true,
          text: 'Häufigkeit der gewählten Fehler im ausgewählten Zeitraum'
        },
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true,
            ticks: {
              suggestedMin: 0
            }
          }],
          yAxes: [{
            display: true,
            ticks: {
            suggestedMin: 0,
            suggestedMax: 12
            },
            scaleLabel: {
              display: true,
              labelString: 'Anzahl der Fehler'
            }
          }]
        },
        animation: {
          duration: 1
        }
      }
    }); 
    this.mychart2 =  new Chart('canvas-rigth', {
      type: 'line',
      data: {
        labels: this.listTime,
        datasets: [
          {
            data: this.listId,
            borderColor: '#3cba9f',
            fill: false
        }
        ]
      },
      options: {
        title: {
          display: true,
          text: 'Zeitlicher Verlauf der Fehler'
        },
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            type: 'time',
            time: {
              displayFormats: {
                'hour': 'DD.MM HH:mm',
                'day': 'DD.MM',
                'week': 'DD.MM',
                'month': 'DD.MM.yyy',
                'quarter': 'DD.MM.yyy',
                'year': 'DD.MM.yyy',
              }
            },
            display: true,
            ticks: {
              major: {
                fontStyle: 'bold'
              }
            },
            scaleLabel: {
              display: true,
              labelString: 'Datum / Uhrzeit'
            }
          }],
          yAxes: [{
            display: true,
            ticks: {
              min: 0,
              max: 8,
              stepSize: 1
            },
            scaleLabel: {
              display: true,
              labelString: 'Fehler-IDs'
            }
          }]
        },
        animation: {
          duration: 1
        },
        hover: {
            animationDuration: 0,
        },
        responsiveAnimationDuration: 0,
        showLines: false
      }
    });
  }


  async sortJSON(data, key) {
    return data.sort(function(a, b) {
      var x = a[key]; var y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  }

  async updateData () {

    if (this.selectedIds.length == 0) {
      this.listData = [];
      this.listLabel = [];
      this.mychart1.data.labels = this.listLabel;
      this.mychart1.data.datasets[0].data = this.listData;
      this.listTime = [];
      this.listId = [];
      this.mychart2.data.labels = this.listTime;
      this.mychart2.data.datasets[0].data = this.listId;
      this.mychart1.update();
      this.mychart2.update();
      return
    }

    this.sortJSON(this.selectedIds, 'id');

    let answer = await this.errorService.getArchiv1(this.selectedIds, this.normalizedTimespanBegin, this.normalizedTimespanEnd);
    this.listLabel = [];
    this.listData = [];
    this.listTime = [];
    this.listId = [];
    for (let option of this.selectedIds){
      this.listLabel.push('Fehler-ID ' + option.id);
      var counter = 0;
      for (let element of answer){
        if (option.id == element.errorId){
          counter++;
        }
      }
      this.listData.push(counter);
    }
    for (let eintrag of answer){
      this.listId.push(eintrag.errorId);
      this.listTime.push(eintrag.createdAt)
    }

    this.mychart1.data.labels = this.listLabel;
    this.mychart1.data.datasets[0].data = this.listData;
    this.mychart2.data.labels = this.listTime;
    this.mychart2.data.datasets[0].data = this.listId;
    this.mychart1.update();
    this.mychart2.update();
  }

  async reset() {
    this.listData = [];
    this.listLabel = [];
    this.selectedIds = [];
    this.mychart1.data.labels = this.listLabel;
    this.mychart1.data.datasets[0].data = this.listData;
    this.listTime = [];
    this.listId = [];
    this.mychart2.data.labels = this.listTime;
    this.mychart2.data.datasets[0].data = this.listId;
    this.mychart1.update();
    this.mychart2.update();
  }

  get normalizedTimespanBegin (): Date {
    return this.timespanBegin ?
      new Date(Date.UTC(this.timespanBegin.getFullYear(), this.timespanBegin.getMonth(), this.timespanBegin.getDate())) :
      null
  }

  get normalizedTimespanEnd (): Date {
    return this.timespanEnd ?
      new Date(Date.UTC(this.timespanEnd.getFullYear(), this.timespanEnd.getMonth(), this.timespanEnd.getDate())) :
      null
  }

}
