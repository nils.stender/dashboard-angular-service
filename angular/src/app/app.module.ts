import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { HttpClientModule }    from '@angular/common/http';
import { AppComponent } from './app.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {PanelModule} from 'primeng/panel';
import {ChartModule} from 'primeng/chart';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {TabMenuModule} from 'primeng/tabmenu';
import { CardModule } from 'primeng/card';
import { ListboxModule } from 'primeng/listbox';
import { CalendarModule } from 'primeng/calendar';



import { LineChartComponent } from './linechartcomp/linechartcomp.component';
import { ArchivComponent } from './archiv/archiv.component';


@NgModule({
  declarations: [
    AppComponent,
    LineChartComponent,
    ArchivComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    PanelModule,
    ChartModule,
    ButtonModule,
    TableModule,
    TabMenuModule,
    CalendarModule,
    ListboxModule,
    CardModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [
      NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
