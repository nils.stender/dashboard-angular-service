import { Timestamp } from "rxjs";

export class Error {
  id: number;
  timestamp: string;
  message: string;
}

export class kafkaJson {
  topic: string;
  value: string;
  offset: number;
  partition: number;
  highWaterOffset: number;
  key: null;
  timestamp: string;
}

export class valueJson {
  id: number;
  time: string;
  sensorIds: number[];
  description: string;
  area: string;
}

export class lineData {
  x: Date;
  y: number;
}

export class mysqlErrHist {
  id: number;
  errorId: number;
  sensoren: string;
  message: string;
  area: string;
  createdAt: Date;
  updatedAt: string;
}


export class mysqlErrHistAR {
  id: number;
  errorId: number;
  sensoren: string;
  message: string;
  area: string;
  createdAt: Date;
  updatedAt: string;
  error_status: string;
}


export class inputBox {
  label: string;
  id: number;
  sensoren: string;
  area: string;
  beschr: string;
}

export class Update {
  id: number;
}
