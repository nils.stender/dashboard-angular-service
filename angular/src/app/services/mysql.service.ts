import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Error, mysqlErrHist, inputBox, mysqlErrHistAR, Update} from './mysql';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  private errorsUrl = 'http://132.187.226.20:8085/api/';  // URL for Server-deployment: 'http://132.187.226.20:8085/api/'    URL for local work: 'http://localhost:8085/api/'
  constructor(
    private http: HttpClient
  ) { }

  getErrors (): Observable<Error[]> {
    return this.http.get<Error[]>(this.errorsUrl +'errorMsg')
  }

  getError(id: number): Observable<Error> {
    const url = `${this.errorsUrl}/${id}`;
    return this.http.get<Error>(url);
  }

  addError (error: Error): Observable<Error> {
    return this.http.post<Error>(this.errorsUrl + 'errorMsg', error, httpOptions);
  }

  getLastFive (): Observable<Error[]> {
    return this.http.get<Error[]>(this.errorsUrl+'lastFive')
  }

  getLast (): Observable<Error[]> {
    return this.http.get<Error[]>(this.errorsUrl+'lastOne')
  }

  deleteError (error: Error | number): Observable<Error> {
    const id = typeof error === 'number' ? error : error.id;
    const url = `${this.errorsUrl+'errorMsg'}/${id}`;
    return this.http.delete<Error>(url, httpOptions);
  }


  updateError (created: String): Promise<any> {
    const url =`${this.errorsUrl+'errorUpdate'}/${created}`;
    console.log(url);
    return this.http.get<any>(url).toPromise();
  }


  getArchiv1 (ids: inputBox[], fromTime? : Date, toTime? : Date): Promise<mysqlErrHist[]> {
    let parameters = [
      ...(fromTime ? ['fromTime=' + (fromTime.getTime() / 1000)] : []),
      ...(toTime ? ['toTime=' + (toTime.getTime() / 1000)] : [])
    ];
    return this.http.get<any[]>(this.errorsUrl+'archiv/' + ids.map(id => id.id).join(',') + '?' + parameters.join('&')).toPromise() as Promise<mysqlErrHist[]>
  }

}
