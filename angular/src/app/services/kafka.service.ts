import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { kafkaJson } from './mysql';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class LiveService {
  private errorsUrl = 'http://132.187.226.20:8089/';  // URL for Server-deployment: 'http://132.187.226.20:8089/'    URL for local work: 'http://localhost:8089/'
  constructor(private http: HttpClient) { }

  getKafkaList (): Observable<kafkaJson[]> {
    return this.http.get<kafkaJson[]>(this.errorsUrl + 'kafkaList')
  }

  getKafkaLast (): Observable<kafkaJson> {
    return this.http.get<kafkaJson>(this.errorsUrl + 'kafkaLast')
  }
}
