import { Component, OnInit, OnDestroy, ViewChild, forwardRef } from '@angular/core';
import { kafkaJson, valueJson, Error } from './services/mysql';
import { LiveService } from './services/kafka.service';
import { Chart, BaseChartDirective } from 'chart.js';
import {ErrorService} from "./services/mysql.service";
import {getProjectAsAttrValue} from "@angular/core/src/render3/node_selector_matcher";


declare var require: any;
export class Headline {
  name: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[LiveService]
})

export class AppComponent implements OnInit, OnDestroy{
  imgname = require('../assets/images/APEtrans.png');
  imgname1 = require('../assets/images/UNIwue2.png');
  imgFertigung: any;
  buttonState = true;
  msg: Error[];
  lastKafkaError: kafkaJson;
  buttonWarn = true;

  title = 'AutoCoP';

  head: Headline = {
    name: 'Fehlerdashboard der Fischermodellfertigung'
  };

  connection;
  connection2;
  connection3;
  errors: kafkaJson[];
  onlyErrors: valueJson[];
  errorsList: kafkaJson[];
  onlyErrorsList: valueJson[];
  listLsg = [0,0,0,0,0,0,0];
  interval: any;
  @ViewChild(forwardRef(() => BaseChartDirective))
  public mychart: BaseChartDirective;
  resetImg: any;
  

  constructor(private liveService: LiveService, private errorService: ErrorService) {}

  ngOnInit() {
    this.kafkaMsg();
    this.mychart =  new Chart('canvas2', {
      type: 'bar',
      data: {
          labels: ['ID 1', 'ID 2', 'ID 3', 'ID 4', 'ID 5', 'ID 6', 'ID 7'],
          datasets: [
          {
              data: this.listLsg,
              borderColor: '#3cba9f'
          }
          ]
      },
      options: {
        title: {
          display: true,
          text: 'Verteilung der letzten 20 Fehler'
        },
        legend: {
            display: false
        },
        scales: {
          xAxes: [{
            display: true,
            ticks: {
                suggestedMin: 0
            }
          }],
          yAxes: [{
            display: true,
            ticks: {
              suggestedMin: 0,
              suggestedMax: 8,
            },
            scaleLabel: {
              display: true,
              labelString: 'Anzahl der Fehler'
            }
          }]
        },
        animation: {
            duration: 1
        }
      }
    });
    this.imgFertigung = require('../assets/images/FertigungOK.png'); // nicht laden am Anfang, falls langsame Internetverbindungen bestehen!
    this.interval = setInterval(() => {
      this.kafkaMsg();
      this.mainFunction();
    }, 1000);
  }  

  ngOnDestroy() {
    this.connection.unsubscribe();
    this.connection2.unsubscribe();
    this.connection3.unsubscribe();

  }

  async makeImgOk () {
    this.resetImg = setTimeout(() => {
      this.imgFertigung = require('../assets/images/FertigungOK.png');
      this.buttonState = true;
      this.buttonWarn = true;
    }, 10000);
  }

  async myStopFunction() {
    clearTimeout(this.resetImg);
  }

  async kafkaMsg() {
    this.connection3 = this.liveService.getKafkaList().subscribe(
      errorsList => {
        if (JSON.stringify(this.errorsList)!=JSON.stringify(errorsList)){
          this.errorsList = errorsList;
          this.onlyErrorsList = [];
          for (let error of this.errorsList) {
            let parts = error.value.split('|');
            this.onlyErrorsList.push({
              id: parseInt(parts[0]),
              time: parts[1].trim(),
              sensorIds: parts[2].split(',').map((i) => {return parseInt(i)}),
              description: parts[3].trim(),
              area: parts[4].trim(),
            });
          }
        }
      }
    );
  }

  async buttonForClose(date : String) {
    this.imgFertigung = require('../assets/images/FertigungOK.png');
    this.buttonState = true;
    this.buttonWarn = true;
    let answer = await this.errorService.updateError(date);
    alert('Fehler wurde behoben!');
  }

  async mainFunction(){

    this.connection2 = this.liveService.getKafkaLast().subscribe(last => { //-> auf kafka zugreifen und schauen was der letzte Fehler ist.
      if (JSON.stringify(this.lastKafkaError)!=JSON.stringify(last)){
        this.myStopFunction();
        this.lastKafkaError = last;
        this.onlyErrors = [];
        let parts = this.lastKafkaError.value.split('|');
        this.onlyErrors.push({
          id: parseInt(parts[0]),
          time: parts[1].trim(),
          sensorIds: parts[2].split(',').map((i) => {return parseInt(i)}),
          description: parts[3].trim(),
          area: parts[4].trim(),
        });
      
        if (this.onlyErrors[0].area.includes('A')){
          this.imgFertigung = require('../assets/images/FertigungA.png');
          this.buttonState = false;
          this.buttonWarn = true;
          this.makeImgOk ()
        } else if (this.onlyErrors[0].area.includes('B')){
          this.imgFertigung = require('../assets/images/FertigungB.png');
          this.buttonState = false;
          this.buttonWarn = false;
          this.makeImgOk ()
        } else if (this.onlyErrors[0].area.includes('C')){
          this.imgFertigung = require('../assets/images/FertigungC.png');
          this.buttonState = false;
          this.buttonWarn = true;
          this.makeImgOk ()
        } else if (this.onlyErrors[0].area.includes('D')){
          this.imgFertigung = require('../assets/images/FertigungD.png');
          this.buttonState = false;
          this.buttonWarn = false;
          this.makeImgOk ()
        }
      }
    });


    this.connection = this.liveService.getKafkaList().subscribe(message => {
      if (JSON.stringify(this.errors)!=JSON.stringify(message)){
        this.errors = message;
        this.onlyErrors = [];
        for (let error of this.errors) {
          let parts = error.value.split('|');
          this.onlyErrors.push({
          id: parseInt(parts[0]),
          time: parts[1].trim(),
          sensorIds: parts[2].split(',').map((i) => {return parseInt(i)}),
          description: parts[3].trim(),
          area: parts[4].trim(),
          });
        }
        var eins = 0;
        var zwei = 0;
        var drei = 0;
        var vier = 0;
        var funf = 0;
        var sechs = 0;
        var sieben = 0;
        for (let element of this.onlyErrors){
            if (element.id == 1){
              eins++;
            } else if (element.id == 2){
              zwei++;
            } else if (element.id == 3){
              drei++;
            } else if (element.id == 4){
              vier++;
            } else if (element.id == 5){
              funf++
            } else if (element.id == 6){
              sechs++
            } else if (element.id == 7){
              sieben++
            }
        }
        this.listLsg[0] = eins;
        this.listLsg[1] = zwei;
        this.listLsg[2] = drei;
        this.listLsg[3] = vier;
        this.listLsg[4] = funf;
        this.listLsg[5] = sechs;
        this.listLsg[6] = sieben;

        this.mychart.update()
      }
    });
  }
}
