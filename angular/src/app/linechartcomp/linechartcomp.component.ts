import { Component, OnInit, ViewChild, forwardRef } from '@angular/core';
import { Chart,BaseChartDirective } from 'chart.js';
import { LiveService } from '../services/kafka.service';
import { valueJson, kafkaJson, lineData } from '../services/mysql';


@Component({
  selector: 'app-linechart',
  templateUrl: './linechartcomp.component.html',
  styleUrls: ['./linechartcomp.component.css'],
  providers:[LiveService]
})
export class LineChartComponent implements OnInit {

@ViewChild(forwardRef(() => BaseChartDirective))
public chart: BaseChartDirective;
onlyErrors: valueJson[];
connection;
interval: any;
errors: kafkaJson;
timeList: any[];
idList: any[];


constructor(private liveService: LiveService) {
    this.timeList = [];
    this.idList = [];
}

    ngOnInit() {
        this.chart = new Chart('canvas', {
          type: 'line',
          data: {
            labels: this.timeList,
            datasets: [
              {
                data: this.idList,
                borderColor: '#3cba9f',
                fill: false
              }
            ]
          },
          options: {
            elements: {
              line: {
                tension: 0,
              }
            },
            title: {
              display: true,
              text: 'Aktuelle Fehler-IDs'
            },
            legend: {
              display: false
            },
            scales: {
              xAxes: [{
                display: true,
                ticks: {
                }
              }],
              yAxes: [{
                display: true,
                ticks: {
                  min: 0,
                  max: 8,
                  stepSize: 1
                },
                scaleLabel: {
                  display: true,
                  labelString: 'Fehler-IDs'
                }
              }]
            },
            animation: {
                duration: 0
            },
            hover: {
                animationDuration: 0,
            },
            responsiveAnimationDuration: 0,
            showLines: false
          }
        });
        this.interval = setInterval(() => {
          this.mainFunction();
        }, 1000);
    }

    ngOnDestroy() {
        this.connection.unsubscribe();
    }


    async mainFunction(){
        this.connection = this.liveService.getKafkaLast().subscribe(message => { // mit kafkaLast -> nur das letzte Element, Probleme, wenn mehrere gleichzeitig kommen!!
            if (JSON.stringify(this.errors)!=JSON.stringify(message)){
                this.errors = message;
                this.onlyErrors = [];
                let parts = this.errors.value.split('|');
                this.onlyErrors.unshift({
                    id: parseInt(parts[0]),
                    time: parts[1].trim().substring(11,19),
                    sensorIds: parts[2].split(',').map((i) => {return parseInt(i)}),
                    description: parts[3].trim(),
                    area: parts[4].trim(),
                });
                var xy = this.onlyErrors[0].time.substring(this.onlyErrors[0].time.indexOf(' '),this.onlyErrors[0].time.length)
                this.timeList.push(xy);
                this.idList.push(this.onlyErrors[0].id);
                if (this.timeList.length >= 9){
                    this.timeList.shift();
                    this.idList.shift();
                }

                this.chart.update()
            }
        });
    }
}
