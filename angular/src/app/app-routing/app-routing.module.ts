import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArchivComponent } from '../archiv/archiv.component';


const routes: Routes = [
  {path: 'dashboard', component: ArchivComponent},
  {
     path: '',
     redirectTo: '',
     pathMatch: 'full'
   }, 
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
