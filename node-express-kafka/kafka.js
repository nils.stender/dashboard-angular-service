//run with: node kafka.js
var kafka = require('kafka-node');
var express = require('express');
var app = express();

// Test-Table, um die Fehlermeldungen in einer MySql DB zu speichern --> nicht weiter verfolgt Stand: 27.12.
// CREATE TABLE errorHistories (id INT AUTO_INCREMENT, message VARCHAR(100), createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP, updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY (id));

const cors = require('cors');
const corsOptions = {
  optionsSuccessStatus: 200
};

app.use(cors(corsOptions));

Consumer = kafka.Consumer;
client = new kafka.KafkaClient({kafkaHost: '132.187.226.20:9092'});
consumer = new Consumer(
    client,
    [
        { topic: 'PJSCEP4'}
    ]
);

var list = [];

console.log('gehts?');
app.get('/kafkaList', function (req, res) {
  res.send(list)
});
app.get('/kafkaLast', function (req, res) {
  res.send(list[0])
});

app.listen(parseInt(process.env.PORT) || 8089, '0.0.0.0', () => {
    console.log("App listening at http://0.0.0.0:8089 || 80");
  consumer.on('message', function (message) {
    list.unshift(message);
    list = list.slice(0,20);
    console.log(message);
  });
});

// Zum Testen, ob die Bilder sich ändern wird mit dem SQL Statement ein Fehler im Bereich A in die DB eingefügt.
// INSERT INTO historie.errors VALUES (NULL,'01:37','Fehler Bereich A',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);SELECT * FROM historie.errors;
