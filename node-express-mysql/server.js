var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());

const cors = require('cors');
const corsOptions = {
  optionsSuccessStatus: 200
};

app.use(cors(corsOptions));

const db = require('./app/config/db.config.js');
  
// force: true will drop the table if it already exists

//db.sequelize.sync({force: true}).then(() => {
//  console.log('Drop and Resync with { force: true }');
//  initial()
// });

require('./app/route/errorTable.route.js')(app); 
 
// Create a Server
var server = app.listen(parseInt(process.env.PORT) || 8085, '0.0.0.0', function () {
 
  let host = server.address().address;
  let port = server.address().port;

  console.log("App listening at http://%s:%s", host, port);
});

function initial(){

  let errorList = [
    {
      id: 1,
      timestamp: "20:12",
      message: "Fehler Bereich B"
    },
    {
      id: 2,
      timestamp: "20.15",
      message: "Fehler Bereich D"
    },
    {
      id: 3,
      timestamp: "20:31",
      message: "Fehler Bereich A"
    },
    {
      id: 4,
      timestamp: "20:45",
      message: "Fehler Bereich B"
    },
    {
      id: 5,
      timestamp: "20:46",
      message: "Fehler Bereich C"
    },
    {
      id: 6,
      timestamp: "20:49",
      message: "Alle Fehler behoben!"
    },
    {
      id: 7,
      timestamp: "20:57",
      message: "Fehler Bereich C"
    },
    {
      id: 8,
      timestamp: "21:23",
      message: "Fehler Bereich A"
    },
    {
      id: 9,
      timestamp: "21:57",
      message: "Fehler Bereich D"
    },
    {
      id: 10,
      timestamp: "22:51",
      message: "Fehler Bereich C"
    },
    {
      id: 11,
      timestamp: "23:11",
      message: "Alle Fehler behoben!"
    }
  ];

  // Init data -> save to MySQL
  const Error = db.testErrors;
  for (let i = 0; i < errorList.length; i++) { 
    Error.create(errorList[i]);  
  }
}