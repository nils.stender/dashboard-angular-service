module.exports = (sequelize, Sequelize) => {
	const Error = sequelize.define('AR_cep_historie', {
	  id: {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true
	  },
	  errorId: {
		  type: Sequelize.INTEGER
	  },
		sensoren: {
		  type: Sequelize.STRING
	  },
		message: {
		  type: Sequelize.STRING
		},
		area: {
		  type: Sequelize.STRING
		},
		createdAt: {
			type: 'TIMESTAMP',
      		defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: false
		},
		updatedAt: {
			type: Sequelize.INTEGER
		},
		error_status: {
	  		type: Sequelize.STRING
		}
	});
	
	return Error;
}