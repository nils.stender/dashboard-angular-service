module.exports = (sequelize, Sequelize) => {
	const Error = sequelize.define('error', {
	  id: {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true
	  },
	  timestamp: {
			type: Sequelize.STRING
	  },
	  message: {
		  type: Sequelize.STRING
		}
	});
	
	return Error;
}