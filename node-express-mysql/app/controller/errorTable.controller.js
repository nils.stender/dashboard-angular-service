const db = require('../config/db.config.js');
const Fehler = db.testErrors;
const cepFehler = db.cepHist;
const { Op } = require('sequelize');
const moment = require('moment');

moment().format();


// Post a Fehler
exports.create = (req, res) => {	
	// Save to MySQL database
	let fehler = req.body;
	Fehler.create(fehler).then(result => {		
		// Send created fehler to client
		res.json(result);
	});
};
 
// Fetch all Fehlers
exports.findAll = (req, res) => {
	Fehler.findAll().then(fehlers => {
	  // Send all fehlers to Client
	  res.json(fehlers);
	});
};

// Fetch top 20 Fehler
exports.findLastFive = (req, res) => {
	Fehler.findAll({limit: 20,attributes: ['id', 'timestamp','message'], where: {}, order: [ [ 'id', 'DESC' ]]} ).then(fehlers => {
	  // Send top 20 Fehler to Client
	  res.json(fehlers);
	});
};

// Fetch last Fehler
exports.findLast = (req, res) => {
	Fehler.findAll({limit: 1,attributes: ['id', 'timestamp','message'], where: {}, order: [ [ 'id', 'DESC' ]]} ).then(fehlers => {
	  // Send top 1 Fehler to Client
	  res.json(fehlers);
	});
};

// Find a Fehler by Id
exports.findById = (req, res) => {	
	Fehler.findById(req.params.fehlerId).then(fehler => {
		res.json(fehler);
	});
};
 
// Update a Fehler
exports.update = (req, res) => {
    const date = req.params.date;
    cepFehler.update(
		{error_status: 'closed'},
	   { where: {updatedAt: date}}
	   ).then(fehler => {
        res.json(fehler);
    });
};
 
// Delete a Fehler by Id
exports.delete = (req, res) => {
	const id = req.params.fehlerId;
	Fehler.destroy({
	  where: { id: id }
	}).then(() => {
	  res.status(200).json({msg:'deleted successfully a fehler with id = ' + id});
	});
};

// Find Fehler starting at a Date
exports.findAtDate = (req, res) => {	
	cepFehler.findAll({
		where: {
			errorId: [req.params.ids.split(',')],
			createdAt: {
				[Op.lt]: new Date(req.query.toTime * 1000),
				[Op.gte]: new Date(req.query.fromTime * 1000)
				
			}
		}
	}).then(fehler => {
		res.json(fehler);
	});
};