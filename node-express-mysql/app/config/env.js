const env = {
  database: 'historie',
  username: 'user1',
  password: 'secret',
  host: '132.187.226.20',
  port: '3306',
  dialect: 'mysql',
  pool: {
	  max: 10,
	  min: 0,
	  acquire: 30000,
	  idle: 10000
  }
};

module.exports = env;