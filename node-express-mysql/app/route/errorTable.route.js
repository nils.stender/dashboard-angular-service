module.exports = function(app) {
 
    const errorMsg = require('../controller/errorTable.controller.js');
 
    // Create a new Error
    app.post('/api/errorMsg', errorMsg.create);

    // Retrieve all Errors
    app.get('/api/errorMsg', errorMsg.findAll);

    // Retrieve top 20 Errors
    app.get('/api/lastFive', errorMsg.findLastFive);

    // Retrieve top 1 Errors
    app.get('/api/lastOne', errorMsg.findLast);
 
    // Retrieve a single Error by Id
    app.get('/api/errorMsg/:errorId', errorMsg.findById);
 
    // Update a Error with date
    app.get('/api/errorUpdate/:date', errorMsg.update);
 
    // Delete a Error with Id
    app.delete('/api/errorMsg/:errorId', errorMsg.delete);

    // Retrieve Errors starting at a date
    app.get('/api/archiv/:ids', errorMsg.findAtDate);
}