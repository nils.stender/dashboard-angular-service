# Das Repository enthält 3 Projekte:

- Angular Dashboard

  This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.3.
  
  Run `ng serve --open` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
  
  Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.
  
  Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

- Kafka Service

  'cd node-express-kafka' 
  
  'node kafka.js' startet den Service
  

- MySql Service

  'cd node-express-mysql'
  
  'node server.js' startet den Service


# Beschreibung zum Deploy

Eine Beschreibung zum Deploy auf den Server findet sich in der README unter 'cd angular'.
